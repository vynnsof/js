const buttons = document.querySelectorAll(".btn");

document.body.addEventListener('keyup', function(event) {
    buttons.forEach((element) => {
        if (element.innerText === event.key.toUpperCase() || event.key === 'Enter') {
            element.style.backgroundColor = "blue";
        }
    });
});

document.body.addEventListener('keydown', function(event)  {
    buttons.forEach((element)=> {
        element.style.backgroundColor = "black";
    })
});