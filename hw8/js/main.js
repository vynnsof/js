// add Price label for input
let inputLabel = document.createElement('div');
inputLabel.className = 'price-label';
inputLabel.innerText = 'Price';
document.body.append(inputLabel);

// add input
let inputPrice = document.createElement('input');
inputPrice.className = 'price-input';
document.body.append(inputPrice);

let inputPriceError = document.createElement('div');
inputPriceError.className = 'price-input-error';
inputPriceError.innerText = 'Please enter correct price';

let div =  document.querySelector('div');

inputPrice.onfocus = function () {
    this.style.cssText = 'border-color: #1ba074;\n' +
        '    outline: none;\n' +
        '    box-shadow: none;';
};


inputPrice.onblur = function () {
    let price = inputPrice.value;
    if (isNaN(price) || price < 0) {
        inputPrice.classList.add('validate-input');
        document.body.append(inputPriceError);
        inputPrice.value = '';
        return false;
    } else {
        inputPrice.classList.remove('validate-input');
        inputPriceError.remove();
        price = inputPrice.value;
    }

let span =  document.querySelector('span');

div.classList.remove('price-box');
div.classList.add('added-price');
inputPrice.style.color = 'rgb(27, 160, 116)';
document.querySelector('span').innerHTML = price;

};

let small = document.querySelector('small');
small.addEventListener("click", clear);

function clear (){
    div.classList.add('price-box');
    inputPrice.value = '';
    inputPrice.style.сolor = '#000';
};




