// scroll navigation
$('.men-item').on('click', function (event) {
    event.preventDefault();
    let headerHeight = $('header');
    let id = $(this).attr('href');
    let idPosition = $(id).offset().top - headerHeight.height() + 'px';
    $('body,html').animate({'scrollTop': `${idPosition}`}, 1000);
});
// end scroll navigation

// scroll up button
const windowHeight = $(window).height();
$(window).on('scroll', function () {
    if (pageYOffset >= windowHeight) {
        $('#go_up-btn').css('display', 'block');
    } else {
        $('#go_up-btn').css('display', 'none');
    }
});
$('#go_up-btn').on('click', function (event) {
    event.preventDefault();
    $('body,html').animate({'scrollTop': `0`}, 1000);
});
// END scroll up button

// slide Toggle button
$('#slideToggle').on('click', function () {
    $(`#clients`).slideToggle();
});
