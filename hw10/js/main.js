let inputPassword = document.getElementsByTagName('input');
let showPassword = document.getElementsByClassName('icon-password');
let submitPassword = document.getElementById('submit-btn');
let errorPassword = document.getElementById('password_error');

showPassword[0].addEventListener("click", () => {
    if (showPassword[0].classList.contains("fa-eye-slash")) {
        showPassword[0].classList.remove("fa-eye-slash");
        showPassword[0].classList.add("fa-eye");
        inputPassword[0].setAttribute('type', 'text');
    } else {
        showPassword[0].classList.remove("fa-eye");
        showPassword[0].classList.add("fa-eye-slash");
        inputPassword[0].setAttribute('type', 'password');
    }
});
showPassword[1].addEventListener("click", () => {
    if (showPassword[1].classList.contains("fa-eye-slash")) {
        showPassword[1].classList.remove("fa-eye-slash");
        showPassword[1].classList.add("fa-eye");
        inputPassword[1].setAttribute('type', 'text');
    } else {
        showPassword[1].classList.remove("fa-eye");
        showPassword[1].classList.add("fa-eye-slash");
        inputPassword[1].setAttribute('type', 'password');
    }
});


submitPassword.addEventListener('click', () => {
    if (inputPassword[0].value === inputPassword[1].value) {
        alert('You are welcome');
        inputPassword[0].style.border = "1px solid #000";
        inputPassword[1].style.border = "1px solid #000";
        errorPassword.innerText = "";
    } else {
        inputPassword[0].setAttribute('type', 'text');
        inputPassword[0].style.border = "2px solid red";
        inputPassword[1].setAttribute('type', 'text');
        inputPassword[1].style.border = "2px solid red";
        errorPassword.innerText = "Нужно ввести одинаковые значения";
    }
});
