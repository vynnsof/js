document.querySelector('.tabs').addEventListener("click", trackTabs);
let tabBody = document.querySelectorAll('.show-tab');

for (let i = 1; i < tabBody.length; i++) {
    tabBody[i].style.display = 'none';
}

function trackTabs(event) {
    if (event.target.className == 'tabs-title') {
        let tabCaption = document.getElementsByClassName('tabs-title');
        let getDataTab = event.target.getAttribute('data-tab');

        for (let i = 0; i < tabCaption.length; i++) {
            tabCaption[i].classList.remove('active');
        }
        event.target.classList.add('active');

        for (let i = 0; i < tabBody.length; i++) {
            if (getDataTab == i) {
                tabBody[i].style.display = 'block';
            } else {
                tabBody[i].style.display = 'none';
            }
        }
    }
}
