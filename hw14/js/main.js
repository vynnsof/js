$(function() {

    $('ul.tabs').on('click', 'li:not(.active)', function() {
        $(this).addClass('active').siblings().removeClass('active')
        $(this).closest('.centered-content').find('.show-tab').removeClass('active').eq($(this).index()).addClass('active');
    });

});