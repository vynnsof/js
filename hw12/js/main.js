let currentSlide = 0;
const slides = document.querySelectorAll('.image-to-show');
const stopSlidesShow = document.getElementById('stop');
const playSlidesShow = document.getElementById('play');
let showing = true;

stopSlidesShow.addEventListener('click', sliderStop);
playSlidesShow.addEventListener('click', sliderPlay);

let start = setInterval(nextSlide, 3000);


function nextSlide() {
    slides[currentSlide].className = 'image-to-show';
    currentSlide = (currentSlide + 1) % slides.length;
    slides[currentSlide].className = 'image-to-show current';
}

function sliderStop() {
    clearInterval(start);
    showing = false;
}

function sliderPlay() {
    if (!showing) {
        start = setInterval(nextSlide, 3000);
        showing = true;
    }
}
